<!DOCTYPE html>
 <html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";
        ?>
        <title>Участники</title>
    </head>
    <body>
        <?php
            include_once "includes/header-inc.php";
        ?>
        <div class="container">
        <form action="users_list.php" method="POST" autocomplete="off"  class="form-inline md-form mt-4 form-sm justify-content-center" >
            <i class="fas fa-search" aria-hidden="true"></i>
            <input class="form-control form-control-sm ml-3 w-50 " name="user_name" type="text" 
            placeholder="Поиск" aria-label="Поиск" >
            <button class="btn blue-gradient btn-rounded btn-sm my-0" type="submit">Поиск</button>            
        </form>       
        <?php
            $sql = "SELECT * FROM users WHERE 1";
            if (isset($_POST['user_name']) && $_POST['user_name']!='')
                $sql = $sql." AND name = '".$_POST['user_name']."'";
            include_once "includes/users_list-inc.php";
        ?>
        
        </div>
    </body>
</html>