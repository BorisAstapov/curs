<!DOCTYPE html>
 <html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";
        ?>
        <title>Чаты</title>
    </head>
    <body>
        <?php
            include_once "includes/header-inc.php";
        ?>
        <div class="container">
        <form action="chats_list.php" method="POST" autocomplete="off"  class="form-inline md-form mt-4 form-sm justify-content-center" >
            <i class="fas fa-search" aria-hidden="true"></i>
            <input class="form-control form-control-sm ml-3 w-50 " name="chat_name" type="text" 
            placeholder="Поиск" aria-label="Поиск" >
            <button class="btn blue-gradient btn-rounded btn-sm my-0" type="submit">Поиск</button>            
        </form>  
        <?php
                if (isset($_SESSION['name'])){
                    include_once "includes/create_chat_form-inc.php";
                }
        ?>
        <?php
            $sql = "SELECT * FROM chats WHERE 1";
            if (isset($_POST['chat_name']) && $_POST['chat_name']!='')
                $sql = $sql." AND name = '".$_POST['chat_name']."'";
            include_once "includes/chats_list-inc.php";
        ?>
        
        </div>
    </body>
</html>