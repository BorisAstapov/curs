<!DOCTYPE html>
 <html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";
        ?>
        <title>Работы</title>
    </head>
    <body>
        <?php
            include_once "includes/header-inc.php";
            include_once 'includes/dbh-inc.php';
        ?>
        <div class="container">
        <form action="works.php" method="POST" autocomplete="off"  class="form-inline md-form form-sm mt-4 justify-content-center" >
            <i class="fas fa-search" aria-hidden="true"></i>
            <input class="form-control form-control-sm ml-3 w-50 " name="img_name" type="text" 
            placeholder="Поиск" aria-label="Поиск" >
            <button id="find" class="btn blue-gradient btn-rounded btn-sm my-0" type="submit">Поиск</button>   
            <button class="btn aqua-gradient btn-rounded btn-sm my-0" type="button" data-toggle="collapse" data-target="#MyCollapse" aria-expanded="false" aria-controls="collapseExample">
                Дополнительные параметры    
            </button>
            <div class="collapse w-75" id="MyCollapse">
                <div class="card card-body" >
                <?php 
                    echo '<label class="labell" for="typeInput">Тип:</label>
                    <select name="type" class="browser-default custom-select mb-3" id="typeInput">
                        <option value=""></option>';
                
                    $sql = "SELECT * FROM types;";
                    $result = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_assoc($result)){
                            echo'<option value="'.$row['id'].'">'.$row['type'].'</option>';
                    }
    
                    echo '</select>
                        <label class="labell" for="categoryInput">Катагория:</label>
                        <select name="category" class="browser-default custom-select  mb-3" id="categoryInput">
                            <option value=""></option>';

                    $sql = "SELECT * FROM categories;";
                    $result = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_assoc($result)){
                            echo'<option value="'.$row['id'].'">'.$row['category'].'</option>';
                    }  

                    echo '  </select>
                            <label class="labell">Тэги:</label><br>
                            <textarea id="tagBox"></textarea>
                            <input type="hidden" id="hidden" name="tags">';
                ?> 
                </div>
            </div>           
        </form>       
        <?php
            $sql = "SELECT * FROM images WHERE 1";
            if (isset($_POST['img_name']) && $_POST['img_name']!='')
                $sql = $sql." AND img_name LIKE '".$_POST['img_name']."'";
            if (isset($_POST['type']) && $_POST['type']!='')
                $sql = $sql." AND type_id = ".$_POST['type'];
            if (isset($_POST['category']) && $_POST['category']!='')
                $sql = $sql." AND category_id = ".$_POST['category'];
            if (isset($_POST['tags']) && ($_POST['tags']!='')){
                $tags = explode(',',$_POST['tags']);
                foreach ($tags as $tag) {
                    $sql = $sql." AND id in(
                        SELECT image_id FROM image_tag WHERE tag_id in (
                        SELECT id FROM tags WHERE tag LIKE '$tag'
                    ))";
                }
            }
            include_once "includes/gallery-inc.php";
        ?>
        
        </div>
    </body>

    
<script>

<?php
        $sql = "SELECT * FROM tags;";
        $result = mysqli_query($conn, $sql);
        echo "var tag_list = [";
        $row = mysqli_fetch_assoc($result);
        echo "'".$row['tag']."'";
        while ($row = mysqli_fetch_assoc($result)){
                echo ",'".$row['tag']."'";
        } 
        echo "];"
?>
        $(document).ready(function(){
                $('#tagBox').tagEditor({
                        autocomplete: {
                                delay: 1,
                                position: { collision: 'flip' }, 
                                source: tag_list
                        },
                        forceLowercase: true,
                        placeholder: 'Введите теги ...'
                });
                $("#find").click(function(){
                        tags = $('#tagBox').tagEditor('getTags')[0].tags
                        $("#hidden").val(tags);
                });
        });
</script>

</html>