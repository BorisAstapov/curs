<!DOCTYPE html>
<html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";          
            include_once 'includes/dbh-inc.php';
            $sql = "SELECT * FROM users WHERE name LIKE '".$_GET['name']."'";
            $result = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($result);
            echo '<title>'.$row['name'].'</title>';
        ?>
    </head>
    <body>  
        <?php
            include_once "includes/header-inc.php";
        ?>
        <?php 
        if($row['ban_id']==null){?>
        <main class="container">
            <?php
                echo '
                <div class="d-flex flex-row align-items-center mt-4 mb-2 ml-2 w-100">
                    <img src="'.htmlspecialchars($row['avatar']).'" class="avatar rounded-circle z-depth-0" alt="avatar image">
                    <h5 class="mt-3 ml-1 align-middle">'.$row['name'].'</h5>';
                if (isset($_SESSION['name'])){
                    $name = $_SESSION['name'];
                    if ($name==$_GET['name']){
                        echo '
                            <i class="fas fa-cog fa-2x ml-auto" data-toggle="modal" data-target="#setModal" style="cursor: pointer;"></i>
                            <div class="modal fade" id="setModal" tabindex="-1" role="dialog" aria-labelledby="setModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header">
                                                    <h5 class="modal-title" id="setModalLabel">Настройки</h5>
                                            </div>
                                            <div class="modal-body">
                                            <form id="setForm" action="includes/user_settings-inc.php" method="POST" enctype="multipart/form-data" autocomplete="off">
                                                <label for="loginInput">Логин:</label>                
                                                <input type="text" name="login" id="loginInput" class="form-control mb-3">
                                                <label for="file-with-current1">Аватар:</label>   
                                                <div class="input-default-wrapper mt-3">
                                                    <input type="file" name="file" id="file-with-current1" class="input-default-js">
                                                    <label class="label-for-default-js rounded-right mb-3" for="file-with-current1"><span class="file-with-current1 span-choose-file">Выберете файл</span>
                                                        <div class="float-right span-browse">Найти</div>
                                                    </label>
                                                </div>
                                            </form>
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                                                    <button type="submit" form="setForm" name="Upload" class="btn btn-primary">Сохранить</button>
                                            </div>
                                    </div>
                            </div>
                            </div>
                        ';
                    }else if (isset($_SESSION['admin'])) {
                        echo '
                        <i class="fas fa-gavel fa-2x ml-auto" data-toggle="modal" data-target="#banModal" style="cursor: pointer;"></i>
                        <div class="modal fade" id="banModal" tabindex="-1" role="dialog" aria-labelledby="banModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="banModalLabel">Забанить</h5>
                                        </div>
                                        <div class="modal-body">
                                        <form id="banForm" action="includes/user_ban-inc.php" method="POST" enctype="multipart/form-data" autocomplete="off">
                                            <label for="reason">Причина:</label>                
                                            <textarea name="reason" id="reason" class="md-textarea form-control" rows="6" required></textarea>
                                            <input type="hidden" name="user" value="'.$_GET['name'].'">
                                        </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                                            <button type="submit" form="banForm" name="Upload" class="btn btn-primary">Забанить</button>
                                        </div>
                                </div>
                        </div>
                        </div>
                    ';
                    }
                }
                echo '</div>';
            ?>
            <div style="display:inline-block;">
            <?php
                        $sql = "SELECT * FROM medals WHERE id IN
                        (SELECT medal_id FROM user_medal WHERE user_id = ".$row['id'].")";
                        $result = mysqli_query($conn, $sql);
                        while ($medal = mysqli_fetch_assoc($result)){
                            echo '<img src="'.htmlspecialchars($medal['image']).'" style="width: 32px; height: 32px; margin-right: 5px;" alt="'.$medal['name'].'">';
                        } 
            ?>
            </div>
            <section class="images-links">
                <?php
                if (isset($_SESSION['name'])){
                    $name = $_SESSION['name'];
                    if ($name==$_GET['name']){
                        include_once "includes/upload_form-inc.php";
                    }
                }
                ?>
                <h2 class="ml-3">Работы</h2>
                <?php 
                    echo '<div class="wrapper">';
                    $sql = "SELECT * FROM images WHERE user_id =(
                        SELECT id FROM users WHERE 	name LIKE '".$_GET['name']."'
                        )";
                    include_once "includes/gallery-inc.php";
                ?>
            </section>
        </main><?php 
        }else{
            include_once "includes/ban-inc.php";
        }?>
    </body>
</html>