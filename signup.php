<html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";
        ?>
        <title>Регистрация</title>
        <link rel="stylesheet" type="text/css" href="css/login.css">
    </head>
    <body style="background-image: url('images/bg_sign.jpg');">
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Регестрация</h3>
                    <?php
                        if (isset($_GET['wrong'])){
                            echo '<h6>Ошибка регистации. Попробуйте заново</h6>';
                        }
                    ?>

                </div>
                <div class="card-body">
                    <form action="includes/signup-inc.php" method="POST" autocomplete="off">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" name="username" class="form-control" placeholder="Логин" required>                            
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                            <input type="email" name="email" class="form-control" placeholder="Email" required>                            
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control" placeholder="Пароль" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn float-right login_btn" value="Зарегистрироваться">
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </div>
    </body>
</html>