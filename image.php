<!DOCTYPE html>
<html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";          
            include_once 'includes/dbh-inc.php';
            $sql = "SELECT * FROM images WHERE img_name LIKE '".$_GET['name']."'";
            $result = mysqli_query($conn, $sql);
            $row = mysqli_fetch_assoc($result);
            echo '<title>'.$row['img_name'].'</title>';
            $sql = "SELECT * FROM users WHERE id = ".$row['user_id'];
            $result = mysqli_query($conn, $sql);
            $user_row = mysqli_fetch_assoc($result);
        ?>
    </head>
    <body>  
        <?php
            include_once "includes/header-inc.php";
        ?>
        <main class="container">
            <?php
                echo '
                <div class="d-flex flex-row align-items-center mt-4 mb-2 ml-2 w-100">
                    <img src="'.htmlspecialchars($user_row['avatar']).'" class="avatar rounded-circle z-depth-0" alt="avatar image">
                    <h5 class="mt-3 ml-1 align-middle">'.$user_row['name'].'</h5>
                </div>';
                echo '
                <h2>Название: '.$row['img_name'].'</h2> 
                <h6>Тэги:</h6><div>';
                $sql = "SELECT * FROM tags WHERE id in (
                    SELECT tag_id FROM image_tag WHERE image_id = ".$row['id']."
                )";
                $result = mysqli_query($conn, $sql);
                $count = mysqli_num_rows($result);
                if ($count){
                    while ($tag_row = mysqli_fetch_assoc($result)){
                        echo "<span>".$tag_row['tag'].", </span>";
                    } 
                }else{ 
                    echo "<span>нету</span>";
                }

                echo '</div>
                <img class="w-100" src='.$row["img_full_name"].' alt="'.$row['img_name'].'">             
                ';
                $sql = "SELECT * FROM coments WHERE image_id = ".$row['id'];
                include_once("includes/comment_section-inc.php");
            ?>
        </main>
    </body>
</html>