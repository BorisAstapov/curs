<!DOCTYPE html>
<html>
    <head>
        <?php
            error_reporting(0);
            session_start();
            if ($_SESSION['admin']!=1){
                header("Location: /index.php");
                exit();
            }
            include_once "includes/dependencies-inc.php";          
            include_once 'includes/dbh-inc.php';
        ?>
        <style>
            table, th, td {
                margin-bottom: 50px;
                margin-left: auto;
                margin-right: auto;
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
    <?php
        include_once "includes/header-inc.php";
    ?>
        <form method="POST" autocomplete="off">
        <span>Запрос: </span>
        <input type="text" name="query">
        <input type="submit" name="SubmitButton" value="Отправить запрос">
        <input type="submit" name="SaveDB" value="Сохранить бд">
        <input type="submit" name="Excel" value="Вывести БД в Excel">
        <input type="file" id="fileselect"  name="LoadFile" accept=".sql" style=" width: 0px; height: 0px; overflow: hidden;">
        <input type="button" id="LoadDB" name="LoadDB" value="Загрузить бд">
        <input type="submit" name="Medals" value="Выдать медальки">
        </form><br><br>
        <script>   
            $(document).ready(function(){
                $("#LoadDB").click(function(){      
                    $("#fileselect").trigger('click'); 
                });
                $("#fileselect").change(function(){ 
                    let question = "Вы уверенны что хотите загрузить"+$(this).val();
                    var sure = confirm(question);
                    if(sure)
                        $('form').submit(); 
                    else 
                        $(this).val(null);
                    
                });
            });
        </script>
        
        <?php 

            function print_all_tables(){
                global $conn;
                $sql = "SHOW TABLES";//получить спичок таблиц
                $result = mysqli_query($conn, $sql);
                while ($row = mysqli_fetch_assoc($result)){
                    echo "<table>";
                    $sql = "DESCRIBE ".$row['Tables_in_artdata'];//получить все колонки указанной таблицы
                    $result2 = mysqli_query($conn, $sql);
                    echo "<tr><th colspan='".(mysqli_num_rows($result2))."'>".$row['Tables_in_artdata']."</th></tr>";// mysqli_num_rows($result2) - кол-во полей вернутых последним запросом
                    echo "<tr>";
                    while ($row2 = mysqli_fetch_assoc($result2)){                  
                        echo "<th>".$row2['Field']."</th>";                   
                    }
                    echo "</tr>";
                    $sql = "Select * From ".$row['Tables_in_artdata'];
                    $result2 = mysqli_query($conn, $sql);
                    while ($row2 = mysqli_fetch_assoc($result2)){
                        echo "<tr>";
                        $i =0;
                        foreach($row2 as $inneritem){
                            $fk=check($row['Tables_in_artdata'],array_keys($row2)[$i]);//если таблица ключь записать значение из таблицы на которую ссылается
                            if(!is_null($fk)){
                                echo "<td>".get_for($fk,(int)$inneritem)."</td>";
                            }
                            else{
                                echo "<td>".$inneritem."</td>";
                            }
                            $i++;
                        }
                        echo "</tr>";
                    } 
                    echo "</table>";
                }
            }

            function print_table(){
                global $conn;
                $sql = $_POST["query"];
                $result2 = mysqli_query($conn, $sql);
                echo "<table>";
                $first = true;
                while ($row2 = mysqli_fetch_assoc($result2)){
                    if($first){
                        $colNames = array_keys($row2);
                        echo "<tr><th colspan='".count($colNames)."'>Результат</th></tr>";
                        echo "<tr>";
                        foreach ($colNames as &$col){                  
                            echo "<th>".$col."</th>";                   
                        }
                        echo "</tr>";
                        $first = false;
                    }
                    echo "<tr>";
                    $i =0;
                    foreach($row2 as $inneritem){
                        $fk=check($table,array_keys($row2)[$i]);
                        if(!is_null($fk)){
                            echo "<td>".get_for($fk,(int)$inneritem)."</td>";
                        }
                        else{
                            echo "<td>".$inneritem."</td>";
                        }
                        $i++;
                    }
                    echo "</tr>";
                } 
                echo "</table>";
            }

            function check($table,$column){ 
                $sql = "select referenced_table_name as 'references' from
                information_schema.key_column_usage where
                referenced_table_name is not null and table_name='".$table."' and column_name='".$column."';"; 
                $result = mysqli_query($GLOBALS['conn'], $sql);
                $row = mysqli_fetch_assoc($result);
                return $row['references'];
            }
            
            function get_for($table,$id){
                $sql = "Select * From ".$table." Where id=$id";
                $result = mysqli_query($GLOBALS['conn'], $sql);
                $row = mysqli_fetch_assoc($result);
                $second = array_slice($row, 1, 1);
                return  reset($second);
            }

            if(isset($_POST["SubmitButton"])){
                if ($_POST["query"])
                    print_table();
                else{
                    print_all_tables();
                }
            }

            if(isset($_POST["SaveDB"])){
                $ExportPath = "db_dump/".date('Y-m-d-H-i-s').".sql";
                //$mysqldump_location = " ../../mysql/bin/mysqldump";
                $mysqldump_location = "C:\\xampp\\mysql\\bin\\mysqldump";
                $command = $mysqldump_location.'  -u' .$dbUserName .' -p' .$dbPassword .' ' .$dbName .' > ' .$ExportPath;
                exec($command,$output,$worked);
                switch($worked){
                case 0:
                    echo 'База данных <b>' .$dbName .'</b>Была успешно записана в  '.getcwd().'/' .$ExportPath .'</b>';
                    break;
                case 1:
                    echo 'Ошибка экспорта <b>' .$dbName .'</b>  '.getcwd().'/' .$ExportPath .'</b>';
                    print_r($output);
                    break;
                case 2:
                    echo 'Возникла ошибка, проверте правильномть параметров: <br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$dbName .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$dbUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$dbServerName .'</b></td></tr></table>';
                    break;
                }
            }

            if(isset($_POST["LoadFile"]) && $_POST["LoadFile"] != null){
                $ExportPath = "../../htdocs/artProj/db_dump/".$_POST["LoadFile"];
                //$mysqldump_location = " ../../mysql/bin/mysqldump";
                $mysql_location = "C:\\xampp\\mysql\\bin\\mysql";
                $command = $mysql_location.'  -u' .$dbUserName .' -p' .$dbPassword .' ' .$dbName .' < ' .$ExportPath;
                //echo $command;
                exec($command,$output,$worked);
                switch($worked){
                    case 0:
                        echo 'Информация из файла <b>' .$mysqlImportFilename .'</b> была успешно импортирована в <b>' .$dbName .'</b>';
                        break;
                    case 1:
                        echo 'Возникла ошибка, проверте правильномть параметров:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$dbName .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$dbUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$dbServerName .'</b></td></tr></table>';
                        print_r($output);
                        break;
                }
            }

            include 'vendor/autoload.php';
            use PhpOffice\PhpSpreadsheet\Spreadsheet;

            if(isset($_POST["Excel"])){
                $file = new Spreadsheet();
                global $conn;
                $sql = "SHOW TABLES";
                $result = mysqli_query($conn, $sql);
                while ($row = mysqli_fetch_assoc($result)){
                    $mySheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($file, $row['Tables_in_artdata']);
                    $sql = "DESCRIBE ".$row['Tables_in_artdata'];
                    $result2 = mysqli_query($conn, $sql);
                    $row2 = mysqli_fetch_all($result2, MYSQLI_ASSOC);
                    $row2 = array_column($row2,'Field');
                    $mySheet->fromArray($row2);
                    $sql = "Select * From ".$row['Tables_in_artdata'];
                    $result2 = mysqli_query($conn, $sql);
                    $row2 = mysqli_fetch_all($result2);
                    $mySheet->fromArray($row2, NULL, 'A2');
                    $file->addSheet($mySheet);
                }   
                $file->removeSheetByIndex(0);      
                $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($file);
                $writer->setPreCalculateFormulas(false);
                $writer->save("db_dump/db.xlsx"); 
                echo "База данных успешно записана в файл db.xlsx";            
            }

            if(isset($_POST["Medals"])){
                $sql = "DELETE FROM `user_medal` WHERE 1";
                mysqli_query($GLOBALS['conn'], $sql);
                $sql = "CALL `checkMedals`()";
                mysqli_query($GLOBALS['conn'], $sql);
            }
        ?>
    </body>
</html>