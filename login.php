<html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";
        ?>
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <title>Вход</title>
    </head>
    <body style="background-image: url('images/bg_log.jpg');">
    <div class="container">            
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Вход</h3>
                    <!--<div class="d-flex justify-content-end social_icon">
                        <span><i class="fab fa-facebook-square"></i></span>
                        <span><i class="fab fa-google-plus-square"></i></span>
                        <span><i class="fab fa-twitter-square"></i></span>
                    </div>-->
                    <?php
                        if (isset($_GET['wrong'])){
                            echo '<h6>Неверный пароль или логин</h6>';
                        }
                    ?>
                    
                </div>
                <div class="card-body">
                    <form action="includes/login-inc.php" method="POST">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" name="username" class="form-control" placeholder="Логин" required>
                            
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control" placeholder="Пароль" required>
                        </div>
                        <div class="form-group">
                        <input type="submit" name="submit" class="btn float-right login_btn" value="Войти">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Нет аккаунта?<a href="signup.php" class="button">Зарегистрироваться</a>
                    </div>
      <!--              <div class="d-flex justify-content-center">
                        <a href="#">Забыли пароль?</a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    </body>
</html>