<!DOCTYPE html>
 <html>
    <head>
        <?php
            include_once "includes/dependencies-inc.php";
        ?>
        <title>Главная старница</title>
    </head>
    <body>
        <?php
            include_once "includes/header-inc.php";
        ?>
        <div class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
                <?php
                    include_once "includes/dbh-inc.php";
                    $sql = "SELECT * FROM images ORDER BY RAND() LIMIT 5;";
                    $result = mysqli_query($conn, $sql);
                    $row = mysqli_fetch_assoc($result);
                    echo '
                    <div class="carousel-item active">
                        <div  class="slider_img">
                            <a href="image.php?name='.$row["img_name"].'">
                                <img class="w-100" src="'.$row["img_full_name"].'" alt="'.$row["img_name"].'">
                            <a>
                        </div>
                        <div class="carousel-caption d-none d-md-block">
                            <h5>ArtProj</h5>
                            <p>Онлайн галерея для художников</p>
                        </div>
                    </div>
                    ';
                    while ($row = mysqli_fetch_assoc($result)){
                        echo '
                        <div class="carousel-item">
                            <div  class="slider_img">
                                <a href="image.php?name='.$row["img_name"].'">
                                    <img class="w-100" src="'.$row["img_full_name"].'" alt="'.$row["img_name"].'">
                                <a>
                            </div>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>ArtProj</h5>
                                <p>Онлайн галерея для художников</p>
                            </div>
                        </div>
                        ';
                    }
                ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
    </body>
</html>