
    <div class="col-md-12 justify-content-center">
<?php
    include_once 'dbh-inc.php';
    $sql = $sql." ORDER BY post_time DESC ";
    $result = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($result);
    if ($count<1){               
        echo '<h5 class="mx-auto" >Ничего не найдено</h5>';
    }else{
        $img_count = 20;
        $start = 0;
        if (isset($_GET['page'])) 
            $current = intval($_GET['page']); 
        else 
            $current=1;
        $start = abs(($current-1)*$img_count);
        $sql = $sql." LIMIT $start, $img_count;";
        $result = mysqli_query($conn, $sql);
        for($i = 1; $i <= 5; $i++){
            echo '<div class="row mb-3 ">';
            for($j = 1; $j <= 4; $j++){
                $row = mysqli_fetch_assoc($result);
                if (!$row){
                    echo '</div>';
                    break 2;
                }
                echo '
                <a href="'.$row["img_full_name"].'" 
                data-toggle="lightbox" 
                data-gallery="gallery" 
                class="col-sm-3"
                data-title="'.$row["img_name"].'" 
                data-footer=\'<a href="image.php?name='.$row["img_name"].'">
                    <i class="fas fa-share-square h1"></i></a>';
                if (isset($_SESSION['admin']) && $_SESSION['admin']==1){
                    echo '<a href="includes/drop-inc.php?name='.$row["img_name"].'">
                    <i class="fas fa-minus-circle fa-2x"></i></a>';
                }
                echo '\'>
                    <img src="'.$row["img_full_name"].'" class="img-fluid">   
                </a>                
                ';
            };
            echo '</div>';
        };
        if ($count>$img_count){
            $total_pages = abs($count/$img_count);
            if ($count%$img_count > 0)
                $total_pages+=1;
            echo '<br><div class="row justify-content-center">';
            for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
                echo "<a class=\"mr-2\" href='works.php?page=".$i."'";
                //if ($i==$page)  echo " class='curPage'";
                echo ">".$i."</a> "; 
            }; 
            echo '</div>';
        }
    };
?>
    </div>


<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                    event.preventDefault();
                    $(this).ekkoLightbox({
                        wrapping: false,
                    });
                });
</script>
