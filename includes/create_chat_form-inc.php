<?php

        include_once "dbh-inc.php";
        echo '
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                Создать чат
        </button>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <h5 class="modal-title" id="addModalLabel">Создать чат</h5>
                                </div>
                                <div class="modal-body">
        ';

        echo '<form id="addForm" action="includes/create_chat-inc.php" method="POST" enctype="multipart/form-data" autocomplete="off">
                        <label for="nameInput">Название:</label>                
                        <input type="text" name="name" id="nameInput" class="form-control mb-3" required>
                        <label for="descriptionInput">Описание:</label><br>
                        <textarea id="descriptionInput" name="description" style=" width:100%"></textarea>
                </form>';

        echo '                  </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                                        <button type="submit" id="sendImg" form="addForm" name="Upload" class="btn btn-primary">Создать</button>
                                </div>
                        </div>
                </div>
        </div>       
        ';
?>