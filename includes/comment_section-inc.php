<div class="list-group">
<div class="list-group-item list-group-item-light mt-2"><!--comment_start-->
    Комментарии
</div>
<?php
    if(isset($_SESSION['name'])){
        echo '
        <div class="list-group-item ">
            <div class="row">
                <img src="'.htmlspecialchars($_SESSION['avatar']).'" class="avatar rounded-circle z-depth-0" alt="avatar image">
                <h5 class="mt-3 ml-1 align-middle">'.$_SESSION['name'].'</h5>
            </div>
            <form action="includes/upload_comment-inc.php" method="POST" autocomplete="off">
                <textarea class="w-100 mt-2 " name="text" rows="3" placeholder="Ваш комментарий..."></textarea>
                <button type="submit" class="btn purple-gradient float-right">Отправить</button>
                <input type="hidden" value="'.$_GET['name'].'" name="image">
                <input type="hidden" value="'.$_SESSION['name'].'" name="user">
            <form>
        </div>
        ';
    }
    include_once 'dbh-inc.php';  
    $result = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($result);
    if ($count<1){               
        echo '<div class="list-group-item ">
                <h5 class="mx-auto" >Комментариев нет</h5>
            </div>';
    }else{
        while($row = mysqli_fetch_assoc($result)){
            $sql = "SELECT * FROM users WHERE id = ".$row['user_id'];
            $com_result = mysqli_query($conn, $sql);
            $com_row = mysqli_fetch_assoc($com_result);
            echo '  
            <div class="list-group-item ">
                <div class="row">
                    <img src="'.htmlspecialchars($com_row['avatar']).'" class="avatar rounded-circle z-depth-0" alt="avatar image">
                    <h5 class="mt-3 ml-1 align-middle">'.$com_row['name'].'</h5>
                </div>
                <p>'.$row['text'].'<p>
            </div>
        ';
        }
    }

?>
</div>