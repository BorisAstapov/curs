<?php
 session_start();
?>
<header>

<nav class="mb-1 navbar navbar-expand-lg navbar-dark secondary-color lighten-1">
  <a class="navbar-brand" href="index.php">
    <img src="images/logo.png" width="30" height="30" class="d-inline-block align-top" alt="Logo">
            ArtProj
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Главная
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="works.php">Работы</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="users_list.php">Участники</a>
      </li>
      <?php
      if(isset($_SESSION['name'])){
        echo '
        <li class="nav-item">
          <a class="nav-link" href="chats_list.php">Чаты</a>
        </li>
        ';
      }
      if (isset($_SESSION['admin']) && $_SESSION['admin']==1){
          echo '
          <li class="nav-item">
            <a class="nav-link" href="database.php">База Данных</a>
          </li>
          ';
      }
      ?>
    </ul>

    <?php
        if (isset($_SESSION['name'])){
            $name = $_SESSION['name']; 
            $avatar = $_SESSION['avatar'];        
            echo '
            <ul class="avatar_container navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item avatar dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <img src="'.htmlspecialchars($avatar).'" class="avatar rounded-circle z-depth-0" alt="avatar image">
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
                    <a class="dropdown-item" href="user.php?name='.htmlspecialchars($name).'">Аккаунт</a>
                    <a class="dropdown-item" href="includes/logout-inc.php">Выйти</a>
                </div>
                </li>
            </ul>
            ';
        }else{
            echo '<a class="btn btn-primary" href="login.php">Войти</a>';
        }
    ?>

  </div>
</nav>
</header> 