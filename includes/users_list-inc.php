<div class="list-group">
<?php
  include_once 'dbh-inc.php';
  $result = mysqli_query($conn, $sql);
  $count = mysqli_num_rows($result);
  if ($count<1){               
      echo '<h5 class="mx-auto">Ничего не найдено</h5>';
  }else{
      $start = 0;
      if (isset($_GET['page'])) 
          $current = intval($_GET['page']); 
      else 
          $current=1;
      $start = abs(($current-1)*10);
      $sql = $sql." LIMIT ".$start.", 10;";
      $result = mysqli_query($conn, $sql);
      for($i = 1; $i <= 10; $i++){
        $row = mysqli_fetch_assoc($result);
        if (!$row)
            break;
        echo '<a href="user.php?name='.$row['name'].'" class="list-group-item list-group-item-action">
                <div class="row">
                    <img src="'.htmlspecialchars($row['avatar']).'" class="avatar rounded-circle z-depth-0" alt="avatar image">
                    <h5 class="mt-3 ml-1 align-middle">'.$row['name'].'</h5>
                </div>
              </a>';
      };
      if ($count>10){
          $total_pages = abs($count/10);
          if ($count%10 > 0)
              $total_pages+=1;
          echo '<div class="row justify-content-center">';
          for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
              echo "<a href='works.php?page=".$i."'";
              //if ($i==$page)  echo " class='curPage'";
              echo ">".$i."</a> "; 
          }; 
          echo '</div>';
      }
  };
?>
</div>