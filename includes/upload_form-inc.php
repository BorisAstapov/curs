<?php

        include_once "dbh-inc.php";
        echo '
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                Добавить рисунок
        </button>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <h5 class="modal-title" id="addModalLabel">Добавить рисунок</h5>
                                </div>
                                <div class="modal-body">
        ';

        echo '<form id="addForm" action="includes/upload-inc.php" method="POST" enctype="multipart/form-data" autocomplete="off">
                        <label for="nameInput">Название:</label>                
                        <input type="text" name="img_name" id="nameInput" class="form-control mb-3" required>
                        <div class="input-default-wrapper mt-3">
                                <input type="file" name="file" id="file-with-current2" class="input-default-js" required>
                                <label class="label-for-default-js rounded-right mb-3" for="file-with-current2"><span  class="file-with-current2 span-choose-file">Выберете файл</span>

                                <div class="float-right span-browse">Найти</div>
                                </label>
                        </div>
                        <label for="typeInput">Тип:</label>
                        <select name="type" class="browser-default custom-select mb-3" id="typeInput">';
                    
        $sql = "SELECT * FROM types;";
        $result = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_assoc($result)){
                echo'<option value="'.$row['id'].'">'.$row['type'].'</option>';
        }
        
        echo '</select>
              <label for="categoryInput">Катагория:</label>
              <select name="category" class="browser-default custom-select  mb-3" id="categoryInput">';

        $sql = "SELECT * FROM categories;";
        $result = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_assoc($result)){
                echo'<option value="'.$row['id'].'">'.$row['category'].'</option>';
        }  

        echo '  </select>
                <label>Тэги:</label><br>
                <textarea id="tagBox"></textarea>
                <input type="hidden" id="hidden" name="tags">
                </form>';

        echo '                  </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                                        <button type="submit" id="sendImg" form="addForm" name="Upload" class="btn btn-primary">Добавить</button>
                                </div>
                        </div>
                </div>
        </div>       
        ';
?>

<script>

<?php
        $sql = "SELECT * FROM tags;";
        $result = mysqli_query($conn, $sql);
        echo "var tag_list = [";
        $row = mysqli_fetch_assoc($result);
        echo "'".$row['tag']."'";
        while ($row = mysqli_fetch_assoc($result)){
                echo ",'".$row['tag']."'";
        } 
        echo "];"
?>
        $(document).ready(function(){
                $('#tagBox').tagEditor({
                        autocomplete: {
                                delay: 1,
                                position: { collision: 'flip' }, 
                                source: tag_list
                        },
                        forceLowercase: true,
                        placeholder: 'Введите теги ...'
                });

                $('input[type="file"]').change(function(e){
                        id = $(this).attr('id');
                        fileName = e.target.files[0].name;
                        $("."+id).text(fileName);
                });
                $("#sendImg").click(function(){
                        tags = $('#tagBox').tagEditor('getTags')[0].tags
                        $("#hidden").val(tags);
                });
        });
</script>
