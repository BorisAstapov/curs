<div class="list-group">
<div class="list-group-item list-group-item-light mt-2">
    <?php echo $row['name'] ?>
</div>
<?php
    include_once 'dbh-inc.php';  
    $result = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($result);
    if ($count<1){               
        echo '<div class="list-group-item ">
                <h5 class="mx-auto" >Пустой чат</h5>
            </div>';
    }else{
        while($mes_row = mysqli_fetch_assoc($result)){
            $sql = "SELECT * FROM users WHERE id = ".$mes_row['user_id'];
            $user_result = mysqli_query($conn, $sql);
            $user_row = mysqli_fetch_assoc($user_result);
            echo '  
            <div class="list-group-item ">
                <div class="row">
                    <h5 class="mt-3 ml-1 align-middle">'.$user_row['name'].'</h5>
                    <p class="mt-3 ml-1 align-middle">'.$mes_row['post_time'].'</p>
                </div>
                <p>'.$mes_row['message'].'<p>
            </div>
        ';
        }
    }

    if(isset($_SESSION['name'])){
        echo '
        <div class="list-group-item ">
            <form action="includes/upload_message-inc.php" method="POST" autocomplete="off">
                <textarea class="w-100 mt-2 " name="text" rows="3" placeholder="Введите сообщение"></textarea>
                <button type="submit" class="btn purple-gradient float-right">Отправить</button>
                <input type="hidden" value="'.$_GET['name'].'" name="chat">
                <input type="hidden" value="'.$_SESSION['name'].'" name="user">
            <form>
        </div>
        ';
    }

?>
</div>