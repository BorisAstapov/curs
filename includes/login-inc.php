<?php
    if (isset($_POST['submit'])){

        include_once 'dbh-inc.php';
       
        $name =  mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        
        if (empty($name) || empty($password)){
            header("Location: ../login.php");
            exit();
        }else{
            //cheak name
            $sql = "SELECT * FROM users WHERE name='$name';";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result)<1){               
                header("Location: ../login.php?wrong=true");
                exit();
            }else{
                if ($row = mysqli_fetch_assoc($result)){
                    //de-hasing the password
                    $passCheck = password_verify($password,$row['password']);
                    if ($passCheck == false){
                        header("Location: ../login.php?wrong=true");
                        exit();
                    } elseif ($passCheck == true) {
                        //user banned
                        if ($row['ban_id']==null){
                            //log in user
                            session_start();
                            $_SESSION['name'] = $row['name'];
                            $_SESSION['avatar'] = $row['avatar'];
                            $_SESSION['admin'] = $row['admin'];
                        }
                        header("Location: ../user.php?name=$name");
                        exit();
                    }
                }
            }
        }

    }else{
        header("Location: ../login.php");
        exit();
    }
?>