<?php
    $img_name=$_POST['img_name'];
    $file = $_FILES['file'];

    $fileName = $file['name'];
    $fileTmpName = $file['tmp_name'];
    $fileSize = $file['size'];
    $fileError = $file['error'];
    $fileType = $file['type'];

    $fileExt = explode('.',$fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg','jpeg','png','bmp','gif');

    if (in_array($fileActualExt,$allowed)){
        if ($fileError === 0){
            if ($fileSize <1000000){
                include_once "dbh-inc.php";
                session_start();
                $name = $_SESSION['name'];
                $fileDestination="users/$name/".$fileName;
                $sql = "SELECT * FROM users WHERE name='$name';";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($result);
                
                $sql = "INSERT INTO images (img_name,img_full_name,user_id,post_time,type_id,category_id) VALUES ('$img_name','$fileDestination',".$row['id'].",'".date("Y-m-d H:i:s")."',".$_POST['type'].",".$_POST['category'].");";
                mysqli_query($conn, $sql);
                $img_id = mysqli_insert_id($conn);

                move_uploaded_file($fileTmpName,"../".$fileDestination);

                if ($_POST['tags']!=null){
                    $tags = explode(',',$_POST['tags']);
                    foreach ($tags as $tag) {
                        $sql = "SELECT * FROM tags WHERE tag LIKE '$tag'";
                        $result = mysqli_query($conn, $sql);
                        $row = mysqli_fetch_assoc($result);
                        if ($row){
                            $tag_id = $row['id'];
                        }else{
                            $sql = "INSERT INTO tags (tag) VALUES ('$tag')";
                            $result = mysqli_query($conn, $sql);
                            $tag_id = mysqli_insert_id($conn);
                        }
                        $sql = "INSERT INTO image_tag (image_id, tag_id) VALUES ($img_id,$tag_id)";
                        $result = mysqli_query($conn, $sql);
                    }
                }

                header("Location: ../user.php?name=$name");
                exit();
            }else{
                echo "Файл слишком большой"; 
            }
        }else{
            echo "Ошибка загрузки"; 
        }
    }else{
        echo "Неверный тип файла";
    }    
?>