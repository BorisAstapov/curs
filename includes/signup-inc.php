<?php
    // Show Errors
    echo phpversion();
    error_reporting(E_ALL);
    ini_set('display_errors','On');

    if (isset($_POST['submit'])){
        include_once 'dbh-inc.php';

        $name =  mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $email = mysqli_real_escape_string($conn, $_POST['email']);

        if (empty($name) || empty($password) || empty($email)){
            header("Location: ../signup.php?wrong=error");
            exit();
        }else{
            //check email
            if (false){//!filter_var($email, FILTER_VALIDATA_EMAIL)){
                header("Location: ../signup.php?wrong=error");
                exit();
            }
            else{
                //cheak name
                $sql = "SELECT * FROM users WHERE name='$name';";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result)!=0){               
                    header("Location: ../signup.php?wrong=error");
                    exit();
                }else{
                    //hashing password
                    $hashedpassword = password_hash($password, PASSWORD_DEFAULT);
                    
                    //insert the user into the database
                    $sql = "INSERT INTO users (name,email,password) VALUES ('$name','$email','$hashedpassword');";
                    mysqli_query($conn, $sql);
                    mkdir("../users/$name");
                    //log in user
                    session_start();
                    $_SESSION['name'] = $name;
                    $_SESSION['avatar'] = "profile.png";
                    header("Location: ../user.php?name=$name");
                    exit();
                }
            }
        }
    }else{
        header("Location: ../sigup.php");
        exit();
    }
?>