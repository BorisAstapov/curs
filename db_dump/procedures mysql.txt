DELIMITER $$
CREATE PROCEDURE checkMedals ()
BEGIN
	DECLARE n INT;
	DECLARE i INT;
    DECLARE curr INT;
    SET n = (SELECT COUNT(*) FROM chats);
    SET i=0;
    WHILE i<n DO 
    	SET curr = (SELECT id FROM chats LIMIT i,1);
			CALL checkHasVersion(curr);
		SET i = i + 1;
    END WHILE;       
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE checkImgCount (IN us_id INT) 
BEGIN 
IF countImg(us_id)>0 THEN 
INSERT into `user_medal`(`user_id`, `medal_id`) VALUES(us_id, 2); 
END IF; 
END$$
DELIMITER ; 

DELIMITER $$
CREATE PROCEDURE checkComCount (IN us_id INT) 
BEGIN 
IF countCom(us_id)>0 THEN 
INSERT into `user_medal`(`user_id`, `medal_id`) VALUES(us_id, 3); 
END IF; 
END$$
DELIMITER ; 

DELIMITER $$
CREATE PROCEDURE checkImgCount10 (IN us_id INT) 
BEGIN 
IF countImg(us_id)>9 THEN 
INSERT into `user_medal`(`user_id`, `medal_id`) VALUES(us_id, 7); 
END IF; 
END$$
DELIMITER ; 

DELIMITER $$
CREATE PROCEDURE checkUserImgCom (IN us_id INT) 
BEGIN 
IF imgMaxCom(us_id)>4 THEN 
INSERT into `user_medal`(`user_id`, `medal_id`) VALUES(us_id, 9); 
END IF; 
END$$
DELIMITER ;

CREATE PROCEDURE checkMaxMes (IN us_id INT) 
BEGIN 
IF maxMes(us_id)>4 THEN 
INSERT into `user_medal`(`user_id`, `medal_id`) VALUES(us_id, 10); 
END IF; 
END 